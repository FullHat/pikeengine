cmake_minimum_required(VERSION 3.5)

project(PikeEngine LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_CXX_COMPILER /usr/bin/g++)
set(CMAKE_C_COMPILER /usr/bin/gcc)

add_subdirectory(external/glm)
add_subdirectory(external/glfw)

include_directories(external/imgui-1.88/ external/imgui-1.88/backends/)

set_source_files_properties(external/glad/src/glad.c PROPERTIES LANGUAGE CXX )

set(SOURCE main.cpp
    core/shader.cpp
    core/rectangle.cpp
    core/object.cpp
    window.cpp
    core/camera.cpp

    external/imgui-1.88/imgui.cpp
    external/imgui-1.88/imgui_demo.cpp
    external/imgui-1.88/imgui_draw.cpp
    external/imgui-1.88/imgui_tables.cpp
    external/imgui-1.88/imgui_widgets.cpp

    external/glad/src/glad.c

    external/imgui-1.88/backends/imgui_impl_glfw.cpp
    external/imgui-1.88/backends/imgui_impl_opengl3.cpp)

add_executable(PikeEngine ${SOURCE})

install(TARGETS PikeEngine
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})

target_link_libraries(PikeEngine glfw)

find_package(OpenGL REQUIRED)
target_include_directories(PikeEngine SYSTEM PRIVATE ${OPENGL_INCLUDE_DIR})
target_include_directories(PikeEngine SYSTEM PRIVATE external/glfw/include)
target_include_directories(PikeEngine SYSTEM PRIVATE external/glad/include)
target_include_directories(PikeEngine SYSTEM PRIVATE external)
target_include_directories(PikeEngine SYSTEM PRIVATE core)
target_include_directories(PikeEngine SYSTEM PRIVATE ./)
target_link_libraries(PikeEngine ${OPENGL_gl_LIBRARY})
