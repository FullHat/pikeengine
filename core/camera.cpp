#include "camera.h"
#include "glm/ext/matrix_clip_space.hpp"
#include "glm/ext/matrix_transform.hpp"
#include "window.h"
#include <functional>
#include <iostream>

void camera::poll_events()
{
    static bool init_pr_cursor_pos = true;
    static glm::vec2 pr_cursor_pos;
    if (init_pr_cursor_pos)
    {
        init_pr_cursor_pos = false;
        pr_cursor_pos = window::get_cursor_position();
        return;
    }

    auto cursor_pos = window::get_cursor_position();

    auto diff = pr_cursor_pos - cursor_pos;
    diff.x = -diff.x;
    pr_cursor_pos = cursor_pos;

    const float sensitivity = 0.1f;

    m_yaw += diff.x * sensitivity;
    m_pitch += diff.y * sensitivity;
    if (m_pitch < -89.0f)
    {
        m_pitch = -89.0f;
    }
    else if (m_pitch > 89.0f)
    {
        m_pitch = 89.0f;
    }

    m_fov -= window::mouse_scroll;
    if (m_fov < 1.0)
    {
        m_fov = 1.0;
    }
    else if (m_fov > 45.0)
    {
        m_fov = 45.0;
    }

    window::mouse_scroll = 0.0f;

    glm::vec3 front;
    front.x = std::cos(glm::radians(m_yaw)) * std::cos(glm::radians(m_pitch));
    front.y = std::sin(glm::radians(m_pitch));
    front.z = std::sin(glm::radians(m_yaw)) * std::cos(glm::radians(m_pitch));
    m_front = glm::normalize(front);

    m_vec_right = glm::normalize(glm::cross(m_front, {0.0f, 1.0f, 0.0f}));  // normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
    m_vec_up    = glm::normalize(glm::cross(m_vec_right, m_front));

    window::handler.poll_events(window::current_window.window);
}

glm::mat4 camera::get_view()
{

    m_target = m_pos + m_front;
    return glm::lookAt(m_pos, m_target, m_vec_up);
}

glm::mat4 camera::get_projection()
{
    auto window_width = (float)window::current_window.width;
    auto window_height = (float)window::current_window.height;
    return glm::perspective(glm::radians(m_fov), window_width / window_height, 0.1f, 100.0f);
}

void camera::move_right(const float _diff)
{
    m_pos += m_vec_right * _diff;
}

void camera::move_front(const float _diff)
{
    m_pos += m_front * _diff;
}

void camera::move_to(const glm::vec3 &_pos)
{
    m_pos = _pos;
}

glm::vec3 camera::get_pos() const
{
    return m_pos;
}
camera::camera()
{
    const float delta = 0.1f;
    window::handler.register_event(new press_event{ std::bind(&camera::move_front, this, delta), GLFW_KEY_W });
    window::handler.register_event(new press_event{ std::bind(&camera::move_front, this, -delta), GLFW_KEY_S });
    window::handler.register_event(new press_event{ std::bind(&camera::move_right, this, -delta), GLFW_KEY_A });
    window::handler.register_event(new press_event{ std::bind(&camera::move_right, this, delta), GLFW_KEY_D });
}
