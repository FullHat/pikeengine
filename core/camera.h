#ifndef CAMERA_H
#define CAMERA_H


#include "glm/fwd.hpp"
#include "glm/mat4x4.hpp"
#include "glm/vec3.hpp"
class camera
{
protected:
    glm::vec3 m_pos = {-5.0f, 0.0f, 0.0f};
    glm::vec3 m_target;
    glm::vec3 m_vec_up = {0.0f, 1.0f, 0.0f};
    glm::vec3 m_vec_right;
    glm::vec3 m_direction;
    glm::vec3 m_front;

    float m_yaw = 0.0f;
    float m_pitch = 0.0f;
    float m_fov = 45.0f;
public:
    camera();

    void poll_events();

    glm::mat4 get_view();
    glm::mat4 get_projection();

    void move_right(const float _diff);
    void move_front(const float _diff);

    void move_to(const glm::vec3 &_pos);

    glm::vec3 get_pos() const;
};

#endif // CAMERA_H
