#include "object.h"
#include "camera.h"
#include "glm/ext/scalar_constants.hpp"
#include "glm/fwd.hpp"
#include "glm/matrix.hpp"
#include "rectangle.h"
#include <stdexcept>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "window.h"

#include <iostream>

void object::set_shader_program(const shader *const _shader_program)
{
    m_shader_program = _shader_program;
}

const uint object::init_VBO(const VBO_params_t &_VBO_params, uint _VBO)
{
    bind_VAO();
    glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glVertexAttribPointer(_VBO_params.location, _VBO_params.attrib_count, GL_FLOAT, _VBO_params.normalize, _VBO_params.attrib_step, (GLvoid*)_VBO_params.offset);
    glEnableVertexAttribArray(_VBO_params.location);
    return _VBO;
}
void object::init_VAO(uint _VAO)
{
    m_curr_VAO = _VAO;
}

void object::bind_VAO()
{
    glBindVertexArray(m_curr_VAO);
}

void object::init_VBO_vertexes(const VBO_params_t &_VBO_params, uint _VBO)
{
    init_VBO(_VBO_params, _VBO);
    m_curr_VBO_vertexes = _VBO;
}

void object::init_TBO(const VBO_params_t &_VBO_params, uint _TBO)
{
    m_curr_TBO = init_VBO(_VBO_params, _TBO);
}

void object::init_texture_settings(const texture_params_t &_texture_params, const texture_t _texture_type)
{
    bind_VAO();
    uint texture_id;
    glad_glGenTextures(1, &texture_id);

    glBindTexture(GL_TEXTURE_2D, texture_id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, _texture_params.wrap_s);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, _texture_params.wrap_t);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, _texture_params.min_filter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, _texture_params.mag_filter);

    m_curr_texture_settings[_texture_type] = texture_id;
}

void object::add_texture(const std::string &_texture_source_path, const texture_t _texture_type)
{
    int x, y, ch;
    stbi_set_flip_vertically_on_load(true);
    unsigned char *texture_data = stbi_load(_texture_source_path.c_str(), &x, &y, &ch, 0);
    if (texture_data == nullptr)
    {
        std::cout << "Something interesting in texture initialisation" << std::endl;
        exit(0);
    }

    add_texture(texture_data, _texture_type, x, y, ch);
    stbi_image_free(texture_data);
}
void object::add_texture(unsigned char* const _texture_data, const texture_t _texture_type, const uint _x, const uint _y, const uint _ch, const uint _color_pattern)
{
    bind_VAO();
    glBindTexture(GL_TEXTURE_2D, m_curr_texture_settings[_texture_type]);

    std::string uniform_name;
    switch (_texture_type)
    {
    case DIFFUSE:
        uniform_name = "material.diffuse";
        break;
    case SPECULAR:
        uniform_name = "material.specular";
        break;
    }

    m_shader_program->set_int(uniform_name, _texture_type);

    uint color_pattern;

    if (_color_pattern != 0)
    {
        color_pattern = _color_pattern;
    }
    else
    {
        switch (_ch)
        {
        case 3:
            color_pattern = GL_RGB;
            break;
        case 4:
            color_pattern = GL_RGBA;
            break;
        default:
            throw std::runtime_error("No color pattern");
        }
    }

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _x, _y, 0, color_pattern, GL_UNSIGNED_BYTE, _texture_data);
    glGenerateMipmap(GL_TEXTURE_2D);
}

void object::use_transformation()
{
    m_result_transformation = m_transformation.scaling * m_transformation.scaling_once
            * m_transformation.rotation * m_transformation.rotation_once
            * m_transformation.translation * m_transformation.translation_once;
}

void object::send_data_to_GPU(const gpu_array_data_t &_gpu_array_data)
{
    glBufferData(_gpu_array_data.array_type, _gpu_array_data.data.size() * sizeof(float), _gpu_array_data.data.data(), _gpu_array_data.save_method);
}

void object::render()
{
    if (m_shader_program == nullptr)
    {
        return;
    }

    use_transformation();
    m_transformation.flush_transformations();

    glm::mat4 view          = glm::mat4(1.0f);
    view       = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));
    static camera c;
    c.poll_events();
    m_shader_program->set_vec("material.ambient", {0.2f, 0.2f, 0.2f});
    m_shader_program->set_vec("material.specular", {0.5f, 0.5f, 0.5f});
    m_shader_program->set_float("material.shininess", 64.0f);

    m_shader_program->set_vec("light.ambient",  {0.2f, 0.2f, 0.2f});
    m_shader_program->set_vec("light.diffuse",  {0.5f, 0.5f, 0.5f});
    m_shader_program->set_vec("light.specular", {1.0f, 1.0f, 1.0f});
    float R = 5.0f;
    float t = glfwGetTime();
    m_shader_program->set_vec("light.position", scene_manager::myScene.get_lights()[0]->get_pos());

    m_shader_program->set_matrix("view", c.get_view());
    m_shader_program->set_matrix("projection", c.get_projection());
    m_shader_program->set_matrix("model", m_result_transformation);

    m_shader_program->set_vec("cameraPos", c.get_pos());
    glm::mat3 normalMatrix = glm::transpose(glm::inverse(c.get_view() *  m_result_transformation));
    m_shader_program->set_matrix("normalMatrix", normalMatrix);

    for (auto el : m_curr_texture_settings)
    {
        glActiveTexture(GL_TEXTURE0 + el.first);
        glBindTexture(GL_TEXTURE_2D, el.second);
    }
    glBindVertexArray(m_curr_VAO);
    glDrawArrays(GL_TRIANGLES, 0, m_points.size());
}

void object::translate(const glm::vec3 &_direction)
{
    m_pivot += _direction;
    m_transformation.translation = glm::translate(m_transformation.translation, _direction);
}

void object::rotate(const float _rad_angle)
{
    // Translate to O
    // Rotate
    // Translate to pivot
    m_transformation.rotation = glm::translate(m_transformation.rotation, -m_pivot);
    m_transformation.rotation = glm::rotate(m_transformation.rotation, _rad_angle, glm::vec3(0.0f, 0.0f, 1.0f));
    m_transformation.rotation = glm::translate(m_transformation.rotation, m_pivot);
}

void object::scale(const glm::vec3 &_scale)
{
    m_transformation.scaling = glm::scale(m_transformation.scaling, _scale);
}

void object::translate_once(const glm::vec3 &_direction)
{
    m_pivot += _direction;
    m_transformation.translation_once = glm::translate(transformations_t::default_value, _direction);
}

void object::rotate_once(const float _rad_angle)
{
    // Translate to O
    // Rotate
    // Translate to pivot
    auto matrix = transformations_t::default_value;
    m_transformation.rotation_once = glm::translate(matrix, -m_pivot);
    m_transformation.rotation_once = glm::rotate(m_transformation.rotation_once, _rad_angle, glm::vec3(0.0f, 0.0f, 1.0f));
    m_transformation.rotation_once = glm::translate(m_transformation.rotation_once, m_pivot);
}

void object::scale_once(const glm::vec3 &_scale)
{
    m_transformation.scaling_once = glm::scale(transformations_t::default_value, _scale);
}

void object::move_to(const glm::vec3 &_pos)
{
    translate(_pos - m_pivot);
}

object::object()
{
    unsigned int VAO;
    glad_glGenVertexArrays(1, &VAO);
    init_VAO(VAO);

    m_pivot = {0.0f, 0.0f, 0.0f};
}

object::object(const std::vector<float> &_points)
{
    unsigned int VAO;
    glad_glGenVertexArrays(1, &VAO);
    init_VAO(VAO);

    m_pivot = {0.0f, 0.0f, 0.0f};

    m_points.clear();
    m_points.resize(_points.size() / 3);

    for (int i = 0; i < _points.size(); i += 3)
    {
        m_points.push_back({_points[i], _points[i+1], _points[i+2]});
    }
}

object::object(const std::vector<point_t> &_points, const point_t &_pivot):
    m_points(_points), m_pivot(_pivot)
{
    unsigned int VAO;
    glad_glGenVertexArrays(1, &VAO);
    init_VAO(VAO);
}

scene scene_manager::myScene;

void image::set_shader_program(const shader *const _shader_program)
{
    m_shader_program = _shader_program;
}

const uint image::init_VBO(const VBO_params_t &_VBO_params, uint _VBO)
{
    bind_VAO();
    glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glVertexAttribPointer(_VBO_params.location, _VBO_params.attrib_count, GL_FLOAT, _VBO_params.normalize, _VBO_params.attrib_step, (GLvoid*)_VBO_params.offset);
    glEnableVertexAttribArray(_VBO_params.location);
    return _VBO;
}
void image::init_VAO(uint _VAO)
{
    m_curr_VAO = _VAO;
}

void image::bind_VAO()
{
    glBindVertexArray(m_curr_VAO);
}

void image::init_VBO_vertexes(const VBO_params_t &_VBO_params, uint _VBO)
{
    init_VBO(_VBO_params, _VBO);
    m_curr_VBO_vertexes = _VBO;
}

void image::init_VBO_color(const VBO_params_t &_VBO_params, uint _VBO)
{
    init_VBO(_VBO_params, _VBO);
    m_curr_VBO_color = _VBO;
}

void image::init_EBO(uint _EBO)
{
    bind_VAO();
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _EBO);

    m_curr_EBO = _EBO;

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint) * 6, m_rectangle.get_indexes(), GL_STATIC_DRAW);

}

void image::init_TBO(const VBO_params_t &_VBO_params, uint _TBO)
{
    m_curr_TBO = init_VBO(_VBO_params, _TBO);
}

void image::init_texture_settings(const texture_params_t &_texture_params)
{
    bind_VAO();
    uint texture_id;
    glad_glGenTextures(1, &texture_id);

    glBindTexture(GL_TEXTURE_2D, texture_id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, _texture_params.wrap_s);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, _texture_params.wrap_t);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, _texture_params.min_filter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, _texture_params.mag_filter);

    m_curr_texture_settings = texture_id;
}

void image::add_texture(const std::string &_texture_source_path)
{
    int x, y, ch;
    stbi_set_flip_vertically_on_load(true);
    unsigned char *texture_data = stbi_load(_texture_source_path.c_str(), &x, &y, &ch, 0);
    if (texture_data == nullptr)
    {
        std::cout << "Something interesting in texture initialisation" << std::endl;
        exit(0);
    }

    add_texture(texture_data, x, y, ch);
    stbi_image_free(texture_data);
}
void image::add_texture(unsigned char* const _texture_data, const uint _x, const uint _y, const uint _ch, const uint _color_pattern)
{
    bind_VAO();
    glBindTexture(GL_TEXTURE_2D, m_curr_texture_settings);

    uint color_pattern;

    if (_color_pattern != 0)
    {
        color_pattern = _color_pattern;
    }
    else
    {
        switch (_ch)
        {
        case 3:
            color_pattern = GL_RGB;
            break;
        case 4:
            color_pattern = GL_RGBA;
            break;
        default:
            throw std::runtime_error("No color pattern");
        }
    }

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _x, _y, 0, color_pattern, GL_UNSIGNED_BYTE, _texture_data);
    glGenerateMipmap(GL_TEXTURE_2D);
}

void image::use_transformation()
{
    m_rectangle.use_transformations();
    m_rectangle.calculate_transformed_points();
}

void image::send_data_to_GPU(const gpu_array_data_t &_gpu_array_data)
{
    glBufferData(_gpu_array_data.array_type, _gpu_array_data.data.size() * sizeof(float), _gpu_array_data.data.data(), _gpu_array_data.save_method);
}

void image::render()
{
    if (m_shader_program == nullptr)
    {
        return;
    }

    if (m_rectangle.is_point_in_obj(window::get_cursor_position_float()))
    {
        std::cout << "Hello world!" << std::endl;
    }

    m_shader_program->set_matrix("transform", m_rectangle.get_result_transformation());

    glBindTexture(GL_TEXTURE_2D, m_curr_texture_settings);
    glBindVertexArray(m_curr_VAO);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

void image::translate(const glm::vec2 &_direction_2d)
{
    m_rectangle.translate(_direction_2d);
}

void image::rotate(const float _rad_angle)
{
    m_rectangle.rotate(_rad_angle);
}

void image::scale(const glm::vec2 &_scale)
{
    m_rectangle.scale(_scale);
}

const float *const image::get_normalized_original_vertexes() const
{
    return m_rectangle.get_normalized_original_vertexes();
}

const float *const image::get_original_vertexes() const
{
    return m_rectangle.get_original_vertexes();
}

image::image()
{
    unsigned int VAO;
    glad_glGenVertexArrays(1, &VAO);
    init_VAO(VAO);
}

image::image(const std::array<point_t, 4> &_points):
    m_rectangle(_points)
{
    unsigned int VAO;
    glad_glGenVertexArrays(1, &VAO);
    init_VAO(VAO);
}

image::image(const std::array<point_t, 4> &_points, const point_t &_pivot):
    m_rectangle(_points, _pivot)
{
    unsigned int VAO;
    glad_glGenVertexArrays(1, &VAO);
    init_VAO(VAO);
}

image::image(const float *const _points):
    m_rectangle(_points)
{
    unsigned int VAO;
    glad_glGenVertexArrays(1, &VAO);
    init_VAO(VAO);
}

image::image(const std::vector<float> &_points):
    image(_points.data())
{
    unsigned int VAO;
    glad_glGenVertexArrays(1, &VAO);
    init_VAO(VAO);
}

std::vector<float> image::from_3d_to_2d(const std::vector<float> &_points_3d)
{
    std::vector<float> points_2d;
    points_2d.reserve(_points_3d.size() / 3 * 2);
    for (unsigned int i = 0; i < _points_3d.size(); i += 3)
    {
        points_2d.push_back(_points_3d[i]);
        points_2d.push_back(_points_3d[i+1]);
    }
    return points_2d;
}

button::button():
    image()
{
}

button::button(const std::array<point_t, 4> &_points):
    image(_points)
{
}

button::button(const std::array<point_t, 4> &_points, const point_t &_pivot):
    image(_points, _pivot)
{
}

button::button(const std::vector<float> &_points):
    image(_points)
{
}

button::button(const float *const _points):
    image(_points)
{
}
