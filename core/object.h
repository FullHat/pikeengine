#ifndef OBJECT_H
#define OBJECT_H

#include "rectangle.h"

#include "shader.h"

#include <sys/types.h>
#include <vector>
#include <map>

struct VBO_params_t
{
    uint offset = 0;
    uint attrib_step = 0;
    bool normalize = GL_FALSE;
    uint attrib_count;
    uint location;
};

struct gpu_array_data_t
{
    uint array_type = GL_ARRAY_BUFFER;
    std::vector<float> data;
    uint save_method = GL_STATIC_DRAW;
};

struct texture_params_t
{
    uint wrap_s = GL_CLAMP_TO_BORDER;
    uint wrap_t = GL_CLAMP_TO_BORDER;
    uint min_filter = GL_LINEAR_MIPMAP_LINEAR;
    uint mag_filter = GL_LINEAR;
};

class object
{
protected:
    using point_t = glm::vec3;

    std::vector<point_t> m_points;
    point_t m_pivot;
    struct transformations_t
    {
private:
public:
        constexpr static glm::mat4 default_value = glm::mat4(1.0f);

        glm::mat4 scaling;
        glm::mat4 rotation;
        glm::mat4 translation;

        glm::mat4 scaling_once;
        glm::mat4 rotation_once;
        glm::mat4 translation_once;

        void flush_transformations()
        {
            scaling_once = default_value;
            rotation_once = default_value;
            translation_once = default_value;
        }

        transformations_t(const glm::mat4 &_value_for_all):
            scaling(_value_for_all),
            rotation(_value_for_all),
            translation(_value_for_all),
            scaling_once(_value_for_all),
            rotation_once(_value_for_all),
            translation_once(_value_for_all)
        {
        }

        transformations_t():
            scaling(default_value),
            rotation(default_value),
            translation(default_value),
            scaling_once(default_value),
            rotation_once(default_value),
            translation_once(default_value)
        {
        }
    };
    transformations_t m_transformation;
    glm::mat4 m_result_transformation;

    struct material_t
    {
        glm::vec3 ambient;
        glm::vec3 diffuse;
        glm::vec3 specular;
        float shininess;
    };
    material_t m_material;

public:
    enum texture_t: uint
    {
        DIFFUSE = 0,
        SPECULAR = 1,
    };

public:
    uint m_curr_VAO;
    uint m_curr_VBO_vertexes;
    uint m_curr_TBO;
    std::map<texture_t, uint> m_curr_texture_settings;

public:
protected:
    const uint init_VBO(const VBO_params_t &_VBO_params, uint _VBO);

    const shader *m_shader_program = nullptr;
public:
    object();
    object(const std::vector<float> &_points);
    object(const std::vector<point_t> &_points, const point_t &_pivot = {0.0f, 0.0f, 0.0f});

    void set_shader_program(const shader *const _shader_program);

    void init_VAO(uint _VAO);
    void bind_VAO();
    void init_VBO_vertexes(const VBO_params_t &_VBO_params, uint _VBO);
    void init_TBO(const VBO_params_t &_VBO_params, uint _TBO);
    void init_texture_settings(const texture_params_t &_texture_params, const texture_t _texture_type);
    void add_texture(const std::string &_texture_source_path, const texture_t _texture_type);
    void add_texture(unsigned char* const _texture_data, const texture_t _texture_type, const uint _x, const uint _y, const uint _ch, const uint _color_pattern = 0);

    void use_transformation();

    void send_data_to_GPU(const gpu_array_data_t &_gpu_array_data);

    void render();

    void translate(const glm::vec3 &_direction);
    void rotate(const float _rad_angle);
    void scale(const glm::vec3 &_scale);

    void translate_once(const glm::vec3 &_direction);
    void rotate_once(const float _rad_angle);
    void scale_once(const glm::vec3 &_scale);
    void move_to(const glm::vec3 &_pos);

    const float *const get_normalized_original_vertexes() const;
    const float* const get_original_vertexes() const;
};

class light
{
protected:
    glm::vec3 m_pos;

public:
    glm::vec3 get_pos() const
    {
        return m_pos;
    }

    void set_pos(const glm::vec3 &_pos)
    {
        m_pos = _pos;
    }

    light()
    {

    }
};

class scene
{
protected:
    std::vector<light*> m_lights;

public:
    void register_light(light *_light)
    {
        m_lights.push_back(_light);
    }

    const std::vector<light*>& get_lights() const
    {
        return m_lights;
    }
};

class scene_manager
{
public:
    static scene myScene;
};

class image
{
protected:
    using point_t = glm::vec2;

    simple_rectangle m_rectangle;

public:
    uint m_curr_VAO;
    uint m_curr_VBO_vertexes;
    uint m_curr_VBO_color;
    uint m_curr_EBO;
    uint m_curr_TBO;
    uint m_curr_texture_settings;

public:
protected:
    const uint init_VBO(const VBO_params_t &_VBO_params, uint _VBO);

    const shader *m_shader_program = nullptr;
public:
    image();
    image(const std::array<point_t, 4> &_points);
    image(const std::array<point_t, 4> &_points, const point_t &_pivot);
    image(const std::vector<float> &_points);
    image(const float *const _points);

    void set_shader_program(const shader *const _shader_program);

    void init_VAO(uint _VAO);
    void bind_VAO();
    void init_VBO_vertexes(const VBO_params_t &_VBO_params, uint _VBO);
    void init_VBO_color(const VBO_params_t &_VBO_params, uint _VBO);
    void init_EBO(uint _EBO);
    void init_TBO(const VBO_params_t &_VBO_params, uint _TBO);
    void init_texture_settings(const texture_params_t &_texture_params);
    void add_texture(const std::string &_texture_source_path);
    void add_texture(unsigned char* const _texture_data, const uint _x, const uint _y, const uint _ch, const uint _color_pattern = 0);

    void use_transformation();

    void send_data_to_GPU(const gpu_array_data_t &_gpu_array_data);

    void render();

    void translate(const glm::vec2 &_direction_2d);
    void rotate(const float _rad_angle);
    void scale(const glm::vec2 &_scale);

    const float *const get_normalized_original_vertexes() const;
    const float* const get_original_vertexes() const;

    static std::vector<float> from_3d_to_2d(const std::vector<float> &_points_3d);
};

class button: public image
{
public:
    button();
    button(const std::array<point_t, 4> &_points);
    button(const std::array<point_t, 4> &_points, const point_t &_pivot);
    button(const std::vector<float> &_points);
    button(const float *const _points);
};

#endif // OBJECT_H
