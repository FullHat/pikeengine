#include "rectangle.h"
#include "glm/ext/matrix_transform.hpp"
#include "glm/ext/scalar_constants.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "utils/math.h"

#include <iostream>

bool simple_rectangle::is_point_in_obj(const point_t &_point) const
{
    using pp = non_rotated_point_position;

    if (is_obj_rotated)
    {
        uint intersections_count = 0;
        for (uint i = 0; i < 4; ++i)
        {
            if (is_intersec({m_transformed_points[i], m_transformed_points[i+1]}, _point))
            {
                ++intersections_count;
            }
        }
        if (is_intersec({m_transformed_points[0], m_transformed_points[3]}, _point))
        {
            ++intersections_count;
        }

        return intersections_count % 2 != 0;
    }
    else
    {
        bool result;
        result = _point.x > m_transformed_points[pp::LEFT_TOP].x &&
                 _point.x < m_transformed_points[pp::RIGHT_BOTTOM].x &&
                 _point.y > m_transformed_points[pp::RIGHT_BOTTOM].y &&
                 _point.y < m_transformed_points[pp::LEFT_TOP].y;

        return result;
    }
}

void simple_rectangle::translate(const glm::vec2 &_direction_2d)
{
    m_is_transformed = true;
    m_is_points_calculated = false;
    const auto direction_3d = glm::vec3(_direction_2d.x, _direction_2d.y, 0.0f);
    m_transformation.translation *= glm::translate(m_transformation.translation, direction_3d);
}

void simple_rectangle::rotate(const float _rad_angle)
{
    m_is_transformed = true;
    m_is_points_calculated = false;
    // Translate to O
    // Rotate
    // Translate to pivot
    const auto pivot_3d = glm::vec3(m_pivot.x, m_pivot.y, 0.0f);
    m_transformation.rotation = glm::translate(m_transformation.rotation, -pivot_3d);
    m_transformation.rotation = glm::rotate(m_transformation.rotation, _rad_angle, glm::vec3(0.0f, 0.0f, 1.0f));
    m_transformation.rotation = glm::translate(m_transformation.rotation, pivot_3d);

    is_obj_rotated = true;
}

void simple_rectangle::scale(const glm::vec2 &_scale)
{
    m_is_transformed = true;
    m_is_points_calculated = false;
    const auto scale_3d = glm::vec3(_scale.x, _scale.y, 1.0f);
    m_transformation.scaling *= glm::scale(m_transformation.scaling, scale_3d);
}

void simple_rectangle::use_transformations()
{
    if (m_is_transformed == false)
    {
        return;
    }

    m_is_transformed = false;
    m_result_transformation = m_transformation.scaling * m_transformation.rotation * m_transformation.translation;
}

const glm::mat4& simple_rectangle::get_result_transformation() const
{
    return m_result_transformation;
}

void simple_rectangle::calculate_transformed_points()
{
    if (m_is_points_calculated == true)
    {
        return;
    }

    m_transformed_points.resize(4);
    for (int i = 0; i < m_points.size(); ++i)
    {
        auto vec_4d = glm::vec4 { m_points[i].x, m_points[i].y, 1.0f, 1.0f };
        auto result_vec = m_result_transformation * vec_4d;
        m_transformed_points[i] = {result_vec.x, result_vec.y};
    }

    m_is_points_calculated = true;
}

const unsigned int* const simple_rectangle::get_indexes() const
{
    return m_indexes;
}

const float* const simple_rectangle::get_normalized_original_vertexes() const
{
    return m_normalized_vertexes;
}

const float* const simple_rectangle::get_original_vertexes() const
{
    return glm::value_ptr(*m_points.begin());
}

simple_rectangle::simple_rectangle()
{
}

simple_rectangle::simple_rectangle(const std::array<point_t, 4> &_points, const point_t &_pivot):
    m_points(_points), m_pivot(_pivot)
{
    recaculate_points();
}

simple_rectangle::simple_rectangle(const std::array<point_t, 4> &_points):
    m_points(_points)
{
    using u = simple_rectangle;
    point_t center;
    const auto middle_x = (_points[u::RIGHT_TOP].x - _points[u::LEFT_TOP].x) / 2.0f + _points[u::LEFT_TOP].x;
    const auto middle_y = (_points[u::LEFT_TOP].y - _points[u::LEFT_BOTTOM].y) / 2.0f + _points[u::LEFT_BOTTOM].y;
    center.x = middle_x;
    center.y = middle_y;
    m_pivot = center;

    recaculate_points();
}

simple_rectangle::simple_rectangle(const float *const _points)
{
    for (int i = 0; i < 4; ++i)
    {
        m_points[i] = {_points[i*2], _points[i*2+1]};
    }

    recaculate_points();
}

void simple_rectangle::recaculate_points()
{
    float min_x = m_points[0].x;
    float max_x = m_points[0].x;
    float min_y = m_points[0].y;
    float max_y = m_points[0].y;
    for (auto it = m_points.begin() + 1; it != m_points.end(); ++it)
    {
        if (it->x < min_x)
        {
            min_x = it->x;
        }
        else if (it->x > max_x)
        {
            max_x = it->x;
        }
        if (it->y < min_y)
        {
            min_y = it->y;
        }
        else if (it->y > max_y)
        {
            max_y = it->y;
        }
    }

    using u = simple_rectangle;
    m_points[u::LEFT_TOP] = {min_x, max_y};
    m_points[u::LEFT_BOTTOM] = {min_x, min_y};
    m_points[u::RIGHT_BOTTOM] = {max_x, min_y};
    m_points[u::RIGHT_TOP] = {max_x, max_y};
}
