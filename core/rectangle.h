#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <algorithm>
#include <utility>
#include <vector>

#include "glm/fwd.hpp"
#include "glm/gtc/type_ptr.hpp"
#include <array>

class simple_rectangle
{
    using point_t = glm::vec2;
public:
protected:
    std::array<point_t, 4> m_points;
    std::vector<point_t> m_transformed_points;
    point_t m_pivot;

    enum non_rotated_point_position: unsigned int
    {
        LEFT_TOP,
        LEFT_BOTTOM,
        RIGHT_BOTTOM,
        RIGHT_TOP,
    };
    const unsigned int m_indexes[6] = {
        0, 1, 3,
        1, 2, 3 //
    };
    const float m_normalized_vertexes[8] = {
        1.0f, 1.0f,
        1.0f, 0.0f,
        0.0f, 0.0f,
        0.0f, 1.0f,
    };
    float *m_original_vertexes = nullptr;

    struct transformations_t
    {
    private:
        constexpr static glm::mat4 default_value = glm::mat4(1.0f);
    public:
        glm::mat4 scaling;
        glm::mat4 rotation;
        glm::mat4 translation;

        transformations_t(const glm::mat4 &_value_for_all):
            scaling(_value_for_all),
            rotation(_value_for_all),
            translation(_value_for_all)
        {
        }

        transformations_t():
            scaling(default_value),
            rotation(default_value),
            translation(default_value)
        {
        }
    };
    transformations_t m_transformation;
    glm::mat4 m_result_transformation;
    bool m_is_transformed = false;
    bool m_is_points_calculated = false;

public:
    simple_rectangle();
    simple_rectangle(const std::array<point_t, 4> &_points, const point_t &_pivot);
    simple_rectangle(const std::array<point_t, 4> &_points);
    simple_rectangle(const float *const _points);

    void recaculate_points();

    bool is_obj_rotated = false;
    bool is_point_in_obj(const point_t &_point) const;

    void translate(const glm::vec2 &_direction_2d);
    void rotate(const float _rad_angle);
    void scale(const glm::vec2 &_scale);

    void use_transformations();
    const glm::mat4& get_result_transformation() const;
    void calculate_transformed_points();

    const unsigned int* const get_indexes() const;
    const float* const get_normalized_original_vertexes() const;
    const float* const get_original_vertexes() const;
};

#endif // RECTANGLE_H
