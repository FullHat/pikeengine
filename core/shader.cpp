#include "shader.h"
#include "glm/gtc/type_ptr.hpp"

#include <fstream>
#include <iostream>
#include <istream>
#include <mutex>
#include <sstream>
#include <stdexcept>

uint shader::compile_shader(const char* const _source_code, shader_types _shader_type)
{
    uint shader_id;
    shader_id = glCreateShader((uint)_shader_type);
    glShaderSource(shader_id, 1, &_source_code, NULL);
    glCompileShader(shader_id);

    int success;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        char infoLog[512];
        glGetShaderInfoLog(shader_id, 512, NULL, infoLog);
        std::cout << infoLog << std::endl;
        throw std::runtime_error("ERROR::SHADER::VERTEX::COMPILATION_FAILED");
    };

    m_shaders_id[_shader_type] = shader_id;
    return shader_id;
}

const std::string shader::load_text(const std::string& _file_name) const
{
    std::ifstream fin;
    std::stringstream text_stream;
    std::string text;

    fin.exceptions(std::istream::failbit | std::istream::badbit);

    fin.open(_file_name);
    text_stream << fin.rdbuf();
    fin.close();
    text = text_stream.str();

    return text;
}

shader::shader()
{

}

shader::~shader()
{
    clear_shaders();
}

uint shader::compile_shader_vertex(const std::string &_source_file_name)
{
    return compile_shader(load_text(_source_file_name).c_str(), shader_types::VERTEX);
}

uint shader::compile_shader_fragment(const std::string &_source_file_name)
{
    return compile_shader(load_text(_source_file_name).c_str(), shader_types::FRAGMENT);
}

uint shader::compile_shader_geometry(const std::string &_source_file_name)
{
    return compile_shader(load_text(_source_file_name).c_str(), shader_types::GEOMETRY);
}

uint shader::compile_shader_program()
{
    uint shader_program_id;
    shader_program_id = glCreateProgram();


    for (const auto shader_id : m_shaders_id)
    {
        glAttachShader(shader_program_id, shader_id.second);
    }
    glLinkProgram(shader_program_id);

    int success;
    glGetProgramiv(shader_program_id, GL_LINK_STATUS, &success);
    if(!success)
    {
        char infoLog[512];
        glGetProgramInfoLog(shader_program_id, 512, NULL, infoLog);
        const char* const err_text = "ERROR::SHADER::PROGRAM::LINKING_FAILED\n";
        std::stringstream mess;
        mess << err_text << infoLog;
        throw std::runtime_error(mess.str());
    }

    m_shader_program = shader_program_id;
    return shader_program_id;
}

uint shader::get_shader_vertex()
{
    return m_shaders_id[shader_types::VERTEX];
}

uint shader::get_shader_fragment()
{
    return m_shaders_id[shader_types::FRAGMENT];
}

uint shader::get_shader_program() const
{
    return m_shader_program;
}

void shader::clear_shaders()
{
    for (const auto shader : m_shaders_id)
    {
        glDeleteShader(shader.second);
    }
    m_shaders_id.clear();
}

int shader::get_uniform_location(const std::string &_uniform_name) const
{
    glUseProgram(m_shader_program);
    return glGetUniformLocation(m_shader_program, _uniform_name.c_str());
}

void shader::set_bool(const std::string &_name, const bool _value) const
{
    set_int(_name, (int)_value);
}

void shader::set_int(const std::string &_name, const int _value) const
{
    const auto loc = get_uniform_location(_name);
    glUniform1i(loc, _value);
}
void shader::set_float(const std::string &_name, const float _value) const
{
    const auto loc = get_uniform_location(_name);
    glUniform1f(loc, _value);
}

void shader::set_matrix(const std::string &_name, const glm::mat4 &_value) const
{
    const auto loc = get_uniform_location(_name);
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(_value));
}

void shader::set_matrix(const std::string &_name, const glm::mat3 &_value) const
{
    const auto loc = get_uniform_location(_name);
    glUniformMatrix3fv(loc, 1, GL_FALSE, glm::value_ptr(_value));
}

void shader::set_vec(const std::string &_name, const glm::vec3 &_value) const
{
    const auto loc = get_uniform_location(_name);
    glUniform3f(loc, _value.x, _value.y, _value.z);
}
