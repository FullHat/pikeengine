#ifndef SHADER_H
#define SHADER_H

#include "glm/mat4x4.hpp"

#include <string>
#include <sys/types.h>
#include <unordered_map>

#ifdef __APPLE__
#include "glad/glad.h"
#include "GLFW/glfw3.h"
#define GL_SILENCE_DEPRECATION
#else
#endif

class shader
{
private:
    enum class shader_types: uint
    {
        VERTEX = GL_VERTEX_SHADER,
        FRAGMENT = GL_FRAGMENT_SHADER,
        GEOMETRY = GL_GEOMETRY_SHADER,
    };

    std::unordered_map<shader_types, uint> m_shaders_id;

    uint compile_shader(const char* const _source_code, shader_types _shader_type);

    const std::string load_text(const std::string& _file_name) const;

    uint m_shader_program = 0;
public:
    shader();
    ~shader();

    uint compile_shader_vertex(const std::string &_source_file_name);
    uint compile_shader_fragment(const std::string &_source_file_name);
    uint compile_shader_geometry(const std::string &_source_file_name);

    uint compile_shader_program();

    void clear_shaders();

    uint get_shader_vertex();
    uint get_shader_fragment();
    uint get_shader_program() const;

    int get_uniform_location(const std::string &_uniform_name) const;

    void set_bool(const std::string &name, const bool value) const;
    void set_int(const std::string &name, const int value) const;
    void set_float(const std::string &name, const float value) const;
    void set_matrix(const std::string &name, const glm::mat4 &value) const;
    void set_matrix(const std::string &name, const glm::mat3 &value) const;
    void set_vec(const std::string &name, const glm::vec3 &value) const;
};

#endif // SHADER_H
