#version 330 core
out vec4 FragColor;

in vec2 texCoord_geom;
in vec3 normal;
in vec3 fragPos;

uniform mat3x3 normalMatrix;
uniform vec3 cameraPos;

struct Material {
    sampler2D diffuse;
    sampler2D specular;
    float shininess;
};

struct Light {
    vec3 position;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

uniform Material material;
uniform Light light;

void main()
{
    // ambient
    vec3 ambient  = light.ambient * texture(material.diffuse, texCoord_geom).rgb;

    // common
   vec3 objColor = vec3(texture(material.diffuse, texCoord_geom));

    // diffuse
    vec3 lightDir = normalize(light.position - fragPos);
    vec3 normOnMatrix = normalize(vec3(vec4(normalMatrix * normal, 0.0)));
    float diff = max(dot(normOnMatrix, lightDir), 0.0);
    vec3 diffuse = light.diffuse * diff * texture(material.diffuse, texCoord_geom).rgb;

    // specular
    vec3 viewDir = normalize(cameraPos - fragPos);
    vec3 reflectDir = reflect(-lightDir, normOnMatrix);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = light.specular * spec * texture(material.specular, texCoord_geom).rgb;

   vec3 result = ambient + diffuse + specular;
   FragColor = vec4(result, 1.0f);
}
