#version 330 core
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in VS_OUT {
    vec2 texCoord;
    vec3 fragPos;
} gs_in[];

out vec2 texCoord_geom;
out vec3 normal;
out vec3 fragPos;

vec3 calculate_normal(vec3 a, vec3 b1, vec3 b2) {
    vec3 vec_1 = a - b1;
    vec3 vec_2 = b2 - b1;
    return normalize(cross(vec_1, vec_2));
}

void process_vertex(int index) {
    normal = calculate_normal(vec3(gl_in[0].gl_Position), vec3(gl_in[1].gl_Position), vec3(gl_in[2].gl_Position));
    texCoord_geom = gs_in[index].texCoord;
    gl_Position = gl_in[index].gl_Position;
    fragPos = gs_in[index].fragPos;
    EmitVertex();
}

void main() {
    process_vertex(0);
    process_vertex(1);
    process_vertex(2);
    EndPrimitive();
}
