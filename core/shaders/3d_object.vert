#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexture;

out VS_OUT {
    vec2 texCoord;
    vec3 fragPos;
} vs_out;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    vs_out.texCoord = vec2(aTexture.x, 1.0 - aTexture.y);
    vs_out.fragPos = vec3(model * vec4(aPos, 1.0f));
    gl_Position = projection * view * vec4(vs_out.fragPos, 1.0f);
}
