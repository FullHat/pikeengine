#ifndef SHADER_PRESETS_H
#define SHADER_PRESETS_H

#include "object.h"

#include <sys/types.h>

struct param_presets_t
{
    VBO_params_t vbo_v;
    VBO_params_t vbo_c;
    VBO_params_t vbo_t;
    gpu_array_data_t vbo_gpu;
};

/*
VBO_params_t vbo_v
{
    0,       // < offset
    0,       // < attrib step
    GL_FALSE,// < normalize
    2,       // < attrib count
    0,       // < location
};

VBO_params_t vbo_c
{
    0,       // < offset
    0,       // < attrib step
    GL_FALSE,// < normalize
    4,       // < attrib count
    1,       // < location
};

VBO_params_t vbo_t
{
    0,       // < offset
    0,       // < attrib step
    GL_FALSE,// < normalize
    2,       // < attrib count
    2,       // _v < location
};

gpu_array_data_t vbo_gpu
{
    GL_ARRAY_BUFFER,// array type
    nullptr,        // data
    GL_STATIC_DRAW, //save method
};
*/

struct simple_texture_t: public param_presets_t
{
    simple_texture_t():
        param_presets_t{{0, 0, GL_FALSE, 2, 0},
                        {0, 0, GL_FALSE, 4, 1},
                        {0, 0, GL_FALSE, 2, 2},
                        {GL_ARRAY_BUFFER, {}, GL_STATIC_DRAW}}
    {
    }
};

struct object_3d_t: public param_presets_t
{
    object_3d_t():
        param_presets_t{{0, 0, GL_FALSE, 3, 0},
                        {},
                        {0, 0, GL_FALSE, 2, 1},
                        {GL_ARRAY_BUFFER, {}, GL_STATIC_DRAW}}
    {
    }
};
const object_3d_t object_3d;
const simple_texture_t simple_texture;

#endif // SHADER_PRESETS_H
