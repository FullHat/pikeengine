#version 330 core
out vec4 FragColor;

in vec4 outColor;
in vec2 outTex;

uniform sampler2D textureTexture;

uniform vec4 color;
void main()
{
   FragColor = texture(textureTexture, outTex);
}
