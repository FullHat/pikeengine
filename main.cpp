#include <cstdlib>
#include <iostream>
#include <iterator>

#include <ctime>
#include <cmath>

#include "glm/trigonometric.hpp"
#include "imgui-1.88/imgui.h"
#include "imgui-1.88/backends/imgui_impl_glfw.h"
#include "imgui-1.88/backends/imgui_impl_opengl3.h"

#include "core/shaders/shader_presets.h"

#include "object.h"

#include "window.h"

using namespace std;

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;


bool is_key_press(GLFWwindow* window, uint key)
{
    return glfwGetKey(window, key) == GLFW_PRESS;
}

void process_input(GLFWwindow* window)
{
    if (is_key_press(window, GLFW_KEY_ESCAPE))
    {
        glfwSetWindowShouldClose(window, true);
    }
    static bool pressed_1 = false;
    if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
    {
        pressed_1 = true;
    }
    if (pressed_1 && glfwGetKey(window, GLFW_KEY_1) == GLFW_RELEASE)
    {
        pressed_1 = false;

        int mode;
        glGetIntegerv(GL_POLYGON_MODE, &mode);
        glPolygonMode(GL_FRONT_AND_BACK, mode == GL_LINE ? GL_FILL : GL_LINE);
    }
}

void init_glad()
{
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "init_glad::init (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == nullptr)" << std::endl;
        glfwTerminate();
        exit(-1);
    }
}



int main()
{
    window::create_window(800, 600);
    GLFWwindow* window = window::current_window.window;

    init_glad();

    shader shader_program;
    shader_program.compile_shader_fragment("simple_texture.frag");
    shader_program.compile_shader_vertex("simple_texture.vert");
    shader_program.compile_shader_program();
    shader_program.clear_shaders();

    std::vector<float> vertexes {
        0.5f,  0.5f,  // top right
         0.5f, -0.5f, // bottom right
        -0.5f, -0.5f, // bottom left
        -0.5f,  0.5f, // top left
    };
    std::vector<float> colors = {
        1.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f,
    };

    button obj(vertexes);
    {
        unsigned int VBO_v, VBO_c, VAO, TBO, EBO;
        glad_glGenBuffers(1, &VBO_v);
        glad_glGenBuffers(1, &VBO_c);
        glad_glGenBuffers(1, &TBO);
        glad_glGenBuffers(1, &EBO);

        // Init VAO
        obj.set_shader_program(&shader_program);

        // Init VBO
        obj.init_VBO_vertexes(simple_texture.vbo_v, VBO_v);
        gpu_array_data_t gad = simple_texture.vbo_gpu;
        gad.data = vertexes;
        obj.send_data_to_GPU(gad);

        // Init VBO colors
        obj.init_VBO_color(simple_texture.vbo_c, VBO_c);
        gad = simple_texture.vbo_gpu;
        gad.data = colors;
        obj.send_data_to_GPU(gad);

        // Init EBO
        obj.init_EBO(EBO);

        // Init TBO
        obj.init_TBO(simple_texture.vbo_t, TBO);
        gad = simple_texture.vbo_gpu;
        const auto ver = obj.get_normalized_original_vertexes();
        gad.data = std::vector<float>{ver, ver + 8};
        obj.send_data_to_GPU(gad);

        // Init textures
        texture_params_t tpt;
        obj.init_texture_settings(tpt);
        obj.add_texture("superman.png");

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    };

    std::vector<float> points
    {
        -0.5f, -0.5f, -0.5f,
         0.5f, -0.5f, -0.5f,
         0.5f,  0.5f, -0.5f,
         0.5f,  0.5f, -0.5f,
        -0.5f,  0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,

        -0.5f, -0.5f,  0.5f,
         0.5f, -0.5f,  0.5f,
         0.5f,  0.5f,  0.5f,
         0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f,  0.5f,
        -0.5f, -0.5f,  0.5f,

        -0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f,  0.5f,
        -0.5f,  0.5f,  0.5f,

         0.5f,  0.5f,  0.5f,
         0.5f,  0.5f, -0.5f,
         0.5f, -0.5f, -0.5f,
         0.5f, -0.5f, -0.5f,
         0.5f, -0.5f,  0.5f,
         0.5f,  0.5f,  0.5f,

        -0.5f, -0.5f, -0.5f,
         0.5f, -0.5f, -0.5f,
         0.5f, -0.5f,  0.5f,
         0.5f, -0.5f,  0.5f,
        -0.5f, -0.5f,  0.5f,
        -0.5f, -0.5f, -0.5f,

        -0.5f,  0.5f, -0.5f,
         0.5f,  0.5f, -0.5f,
         0.5f,  0.5f,  0.5f,
         0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f, -0.5f,
    };
    std::vector<float> tex_p
    {
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,

        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,

        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,

        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,

        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,
        1.0f, 0.0f,
        0.0f, 0.0f,
        0.0f, 1.0f,

        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,
        1.0f, 0.0f,
        0.0f, 0.0f,
        0.0f, 1.0f
    };

    std::vector<float> normals
    {
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,

        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,

         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,

        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f
    };
    object cube(points);
    {
        unsigned int VBO_v, VAO, TBO;
        glad_glGenBuffers(1, &VBO_v);
        glad_glGenBuffers(1, &TBO);

        // Init VAO
        shader shader_program_3d;
        shader_program_3d.compile_shader_fragment("3d_object.frag");
        shader_program_3d.compile_shader_vertex("3d_object.vert");
        shader_program_3d.compile_shader_geometry("3d_object.geom");
        shader_program_3d.compile_shader_program();
        shader_program_3d.clear_shaders();
        cube.set_shader_program(&shader_program_3d);

        // Init VBO
        cube.init_VBO_vertexes(object_3d.vbo_v, VBO_v);
        gpu_array_data_t gad = object_3d.vbo_gpu;
        gad.data = points;
        cube.send_data_to_GPU(gad);

        // Init TBO
        cube.init_TBO(object_3d.vbo_t, TBO);
        gad = object_3d.vbo_gpu;
        gad.data = tex_p;
        cube.send_data_to_GPU(gad);

        // Init textures
        texture_params_t tpt;
        cube.init_texture_settings(tpt, object::DIFFUSE);
        cube.add_texture("container_dif.png", object::DIFFUSE);
        cube.init_texture_settings(tpt, object::SPECULAR);
        cube.add_texture("container_spec.png", object::SPECULAR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

    object sun(points);
    {
        unsigned int VBO_v, VAO, TBO;
        glad_glGenBuffers(1, &VBO_v);
        glad_glGenBuffers(1, &TBO);

        // Init VAO
        shader shader_program_3d;
        shader_program_3d.compile_shader_fragment("3d_object_color.frag");
        shader_program_3d.compile_shader_vertex("3d_object.vert");
        shader_program_3d.compile_shader_program();
        shader_program_3d.clear_shaders();
        sun.set_shader_program(&shader_program_3d);

        // Init VBO
        sun.init_VBO_vertexes(object_3d.vbo_v, VBO_v);
        gpu_array_data_t gad = object_3d.vbo_gpu;
        gad.data = points;
        sun.send_data_to_GPU(gad);

        sun.translate({0.0f, 0.5f, 5.0f});

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    ImGui::StyleColorsDark();
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 330");

    light sunLight;
    scene my_scene;
    scene_manager::myScene = my_scene;
    scene_manager::myScene.register_light(&sunLight);

    glEnable(GL_DEPTH_TEST);
    //obj.translate({0.5f, -0.5f});
    bool demo = true;
    const float delta = 0.01f;
    float t = 0;
    while (!glfwWindowShouldClose(window))
    {
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        ImGui::Begin("Hello, world!");                          // Create a window called "Hello, world!" and append into it.

        ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
        ImGui::End();

        ImGui::Render();

        process_input(window);

        auto s = (std::sin(glfwGetTime() * 1.5) + 1.0f) / 2;
        //auto s = std::abs(std::sin(t += delta));

        //glClearColor(0.5f, s, 0.5f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUseProgram(shader_program.get_shader_program());

        float R = 5.0f;
        float t = glfwGetTime();
        //sun.translate_once({4.97287f, 0.0f, -0.520125f});
        sun.move_to({R*std::sin(t), 0.0f, R*std::cos(t)});
        sunLight.set_pos({R*std::sin(t), 0.0f, R*std::cos(t)});


        obj.use_transformation();
        //obj.render();

        cube.render();
        sun.render();

        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    cout << "Hello World!" << endl;
    return 0;
}
