#ifndef MATH_H
#define MATH_H

#include "glm/gtc/epsilon.hpp"
#include "glm/vec2.hpp"
#include "glm/vector_relational.hpp"
#include "iostream"
#include <algorithm>
#include <cmath>

bool equal_epsilon(const float _a, const float _b, const float _epsilon = 0.0000001f)
{
    return std::abs(_a - _b) < _epsilon;
}

float calculate_k(const glm::vec2 &_p1, const glm::vec2 &_p2)
{
    return (_p1.y - _p2.y) / (_p1.x - _p2.x);
}

float calculate_b(const glm::vec2 &_p, float _k)
{
    return _p.y - _k * _p.x;
}

bool is_intersec(const std::pair<glm::vec2, glm::vec2> &_line, const glm::vec2 &_p)
{
    const float k = calculate_k(_line.first, _line.second);
    const float b = calculate_b(_line.first, k);

    const float b2 = _p.y;

    const float x = (b2 - b) / k;

    bool non_endless_lines = false;
    float max_y, min_y;
    if (_line.first.y < _line.second.y)
    {
        min_y = _line.first.y;
        max_y = _line.second.y;
    }
    else
    {
        min_y = _line.second.y;
        max_y = _line.first.y;
    }
    non_endless_lines = _p.y > min_y && _p.y < max_y;

    return non_endless_lines && x > _p.x;
}

class math
{
public:
    math();
};

#endif // MATH_H
