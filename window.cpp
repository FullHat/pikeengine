#include "window.h"
#include "GLFW/glfw3.h"

#include <iostream>

void window::create_window(const uint _width, const uint _height)
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    GLFWwindow* window = glfwCreateWindow(_width, _height, "LearnOpenGL", NULL, NULL);
    if (window == nullptr)
    {
        std::cout << "main::window (window == nullptr)" << std::endl;
        glfwTerminate();
        exit(-1);
    }

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, window::framebuffer_size_callback);
    glfwSetScrollCallback(window, window::get_mouse_scroll);

    window_data_t window_data;
    window_data.window = window;
    window_data.width = _width;
    window_data.height = _height;

    window::current_window = window_data;
}

glm::vec2 window::get_cursor_position()
{
    double x, y;
    glfwGetCursorPos(window::current_window.window, &x, &y);
    glm::vec2 result = {(float)x, (float)y};
    return result;
}

glm::vec2 window::get_cursor_position_float()
{
    auto pos = window::get_cursor_position();
    float x = window::current_window.width;
    float y = window::current_window.height;
    glm::vec2 result = {(pos.x / x) * 2 - 1, ((y - pos.y) / y) * 2 - 1 };
    return result;
}

void window::get_mouse_scroll(GLFWwindow* window, double xoffset, double yoffset)
{
    window::mouse_scroll = yoffset;
}

void window::framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

input_event_handlers window::get_input_handler()
{
    return window::handler;
}

window_data_t window::current_window;
float window::mouse_scroll;
input_event_handlers window::handler;

window::window()
{

}
