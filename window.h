#ifndef WINDOW_H
#define WINDOW_H

#include "glm/vec2.hpp"
#include <functional>
#include <sys/types.h>
#ifdef __APPLE__
#include "GLFW/glfw3.h"
#define GL_SILENCE_DEPRECATION
#endif

#include <iostream>
#include <vector>

struct window_data_t
{
    unsigned int width;
    unsigned int height;
    GLFWwindow *window;
};

struct event
{
    std::function<void(void)> callback;
    virtual void poll_event(GLFWwindow * const _window) = 0;

    event(std::function<void(void)> _callback):
        callback(_callback)
    {
    }
};

struct press_event: public event
{
    uint key;

    virtual void poll_event(GLFWwindow * const _window) override
    {
        if (glfwGetKey(_window, key) == GLFW_PRESS)
        {
            callback();
        }
    }

    press_event(std::function<void(void)> _callback, const uint _key):
        event(_callback), key(_key)
    {
    }
};

struct click_event: public event
{
    uint key;
    bool pressed = false;

    virtual void poll_event(GLFWwindow * const _window) override
    {
        std::cout << pressed << std::endl;

        if (glfwGetKey(_window, key) == GLFW_PRESS)
            pressed = true;

        if (pressed == true && glfwGetKey(_window, key) == GLFW_RELEASE)
        {
            pressed = false;
            callback();
        }
    }

    click_event(std::function<void(void)> _callback, const uint _key):
        event(_callback), key(_key)
    {
    }
};

class input_event_handlers
{
    std::vector<event*> m_events;

public:
    void register_event(event* const _event)
    {
        m_events.push_back(_event);
    }

    void poll_events(GLFWwindow * const _window)
    {
        for (const auto event : m_events)
        {
            event->poll_event(_window);
        }
    }
};

class window
{
public:
    window();

    static void create_window(const unsigned int _width, const unsigned int _height);

    static window_data_t current_window;

    static glm::vec2 get_cursor_position();
    static glm::vec2 get_cursor_position_float();

    static float mouse_scroll;
    static void get_mouse_scroll(GLFWwindow* window, double xoffset, double yoffset);

    static void framebuffer_size_callback(GLFWwindow* window, int width, int height);

    static input_event_handlers handler;
    static input_event_handlers get_input_handler();
};

#endif // WINDOW_H
